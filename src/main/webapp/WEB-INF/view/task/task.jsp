<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <title>Project</title>
</head>
<body>
<p></p>
<p></p>
    <table border="0" style="width: 80%; border-collapse: collapse; border-style: none; margin-left: auto; margin-right: auto;">
        <tbody>
            <tr>
                <td style="width: 50%; text-align: left;"><a href="/">HOME</a></td>
                <td style="width: 50%; text-align: right;"><a href="/taskcreate">CREATE TASK</a></td>
            </tr>
        </tbody>
    </table>

    <table border="2" style="height: 26px; width: 80%; border-collapse: collapse; border-style: solid; margin-left: auto; margin-right: auto;"><caption>
        <tbody>
            <tr style="height: 18px;">
                <td style="width: 4%; height: 18px;">No</td>
                <td style="width: 27%; height: 18px;">ID</td>
                <td style="width: 16%; height: 18px;">NAME</td>
                <td style="width: 25%; height: 18px;">DESCRIPTION</td>
                <td style="width: 8%; height: 18px;"></td>
                <td style="width: 8%; height: 18px;"></td>
                <td style="width: 8%; height: 18px;"></td>
            </tr>
        <c:forEach var="s" items="${list}">
            <tr style="height: 18px;">
                <td style="width: 4%; height: 18px;">${s.count}</td>
                <td style="width: 27%; height: 18px;">${s.id}</td>
                <td style="width: 16%; height: 18px;">${s.name}</td>
                <td style="width: 25%; height: 18px;">${s.description}</td>
                <td style="width: 8%; height: 18px;"><a href="/taskview?id=${s.id}">view</a></td>
                <td style="width: 8%; height: 18px;"><a href="/taskedit?id=${s.id}">edit</a></td>
                <td style="width: 8%; height: 18px;"><a href="/taskremove?id=${s.id}">remove</a></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</body>
</html>