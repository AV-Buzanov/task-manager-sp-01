<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Project</title>
</head>
<body>
    <spring:form modelAttribute="task" action="/taskmerge">
        <spring:hidden path="id"/>
        Name: <spring:input path="name"/><br>
        Description: <spring:input path="description"/><br>
        Status: <spring:select path="status" items="${statuses}"/><br>
        StartDate: <spring:input type="text" class= "date" path="startDate"/><br>
        FinishDate: <spring:input type="text" class= "date"  path="finishDate"/><br>
        Project: <spring:select path="projectId" items="${projects}" itemValue="id" itemLabel="name"/><br>
        <spring:button>Confirm</spring:button>
    </spring:form>
<p><a href="/tasks">back</a></p>
</body>
</html>