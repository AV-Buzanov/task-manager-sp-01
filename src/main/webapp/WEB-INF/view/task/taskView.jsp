<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Task</title>
</head>
<body>
<table border="2" style="width: 50%; border-collapse: collapse; float: left;">
    <tbody>
        <tr>
            <td style="width: 30%;">id</td>
            <td style="width: 70%;">${task.id}</td>
        </tr>
        <tr>
            <td style="width: 30%;">name</td>
            <td style="width: 70%;">${task.name}</td>
        </tr>
        <tr>
            <td style="width: 30%;">description</td>
            <td style="width: 70%;">${task.description}</td>
        </tr>
        <tr>
            <td style="width: 30%;">create date</td>
            <td style="width: 70%;">${task.createDate}</td>
        </tr>
        <tr>
            <td style="width: 30%;">start date</td>
            <td style="width: 70%;">${task.startDate}</td>
        </tr>
        <tr>
            <td style="width: 30%;">finish date</td>
            <td style="width: 70%;">${task.finishDate}</td>
        </tr>
        <tr>
            <td style="width: 30%;">status</td>
            <td style="width: 70%;">${task.status}</td>
        </tr>
        <tr>
            <td style="width: 30%;">project</td>
            <td style="width: 70%;">${project.name}</td>
        </tr>

    </tbody>
</table>
<p></p>
    <a href="/tasks">back</a>
</body>
</html>