<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
    <title>Project</title>
</head>
<body>
    <table border="2" style="width: 50%; border-collapse: collapse; float: left;">
    <tbody>
        <tr>
            <td style="width: 30%;">id</td>
            <td style="width: 70%;">${project.id}</td>
        </tr>
        <tr>
            <td style="width: 30%;">name</td>
            <td style="width: 70%;">${project.name}</td>
        </tr>
        <tr>
            <td style="width: 30%;">description</td>
            <td style="width: 70%;">${project.description}</td>
        </tr>
        <tr>
            <td style="width: 30%;">create date</td>
            <td style="width: 70%;">${project.createDate}</td>
        </tr>
        <tr>
            <td style="width: 30%;">start date</td>
            <td style="width: 70%;">${project.startDate}</td>
        </tr>
        <tr>
            <td style="width: 30%;">finish date</td>
            <td style="width: 70%;">${project.finishDate}</td>
        </tr>
        <tr>
            <td style="width: 30%;">status</td>
            <td style="width: 70%;">${project.status}</td>
        </tr>
        <tr>
            <td style="width: 30%;">tasks</td>
            <td style="width: 70%;">
        <c:forEach var="s" items="${tasks}">
            ${s.name}<br>
        </c:forEach>
            </td>
        </tr>
<p></p>
    <a href="/projects">back</a>
</tbody>
</table>


</body>
</html>