package ru.buzanov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.buzanov.tm.dto.ProjectDTO;
import ru.buzanov.tm.enumerated.Status;
import ru.buzanov.tm.service.ProjectService;
import ru.buzanov.tm.service.TaskService;

import java.util.Arrays;

@Controller
public class ProjectController {
    @Autowired
    private ProjectService projectService;
    @Autowired
    private TaskService taskService;
    private final String userId = "0fba2676-0c83-4831-8a16-ca35927ef8d9";

    @GetMapping("/projects")
    public String projectList(final Model model) throws Exception {
//        final List<ProjectDTO> list = new ArrayList<>(projectService.findAll(userId));
        model.addAttribute("list", projectService.findAll(userId));
        return "project/project";
    }

    @GetMapping(value = "/projectview")
    public String projectView(final Model model, @RequestParam(name = "id") final String id) throws Exception {
        model.addAttribute("project", projectService.findOne(userId, id));
        model.addAttribute("tasks", taskService.findByProjectId(userId, id));
        return "project/projectView";
    }

    @GetMapping(value = "/projectedit")
    public String projectEdit(final Model model, @RequestParam(name = "id") final String id) throws Exception {
        model.addAttribute("project", projectService.findOne(userId, id));
        model.addAttribute("statuses", Arrays.asList(Status.values()));
        return "project/projectEdit";
    }

    @PostMapping(value = "/merge")
    public String projectMerge(@ModelAttribute(name = "project") final ProjectDTO project) throws Exception {
        projectService.merge(userId, project.getId(), project);
        return "redirect:/projects";
    }

    @GetMapping(value = "/projectremove")
    public String projectRemove(@RequestParam(name = "id") final String id) throws Exception {
        projectService.remove(userId, id);
        return "redirect:/projects";
    }

    @GetMapping(value = "/projectcreate")
    public String projectCreate(final Model model) throws Exception {
        final ProjectDTO project = new ProjectDTO();
        project.setUserId(userId);
        model.addAttribute("project", project);
        model.addAttribute("statuses", Arrays.asList(Status.values()));
        return "project/projectEdit";
    }
}
