package ru.buzanov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.buzanov.tm.dto.TaskDTO;
import ru.buzanov.tm.enumerated.Status;
import ru.buzanov.tm.service.ProjectService;
import ru.buzanov.tm.service.TaskService;

import java.util.Arrays;

@Controller
public class TaskController {
    @Autowired
    private TaskService taskService;
    @Autowired
    private ProjectService projectService;
    private final String userId = "0fba2676-0c83-4831-8a16-ca35927ef8d9";

    @GetMapping("/tasks")
    public String taskList(final Model model) throws Exception {
//        List<TaskDTO> list = new ArrayList<>(taskService.findAll(userId));
        model.addAttribute("list", taskService.findAll(userId));
        return "task/task";
    }

    @GetMapping(value = "/taskview")
    public String taskView(final Model model, @RequestParam(name = "id") final String id) throws Exception {
        final TaskDTO task = taskService.findOne(userId, id);
        model.addAttribute("task", task);
        model.addAttribute("project", projectService.findOne(userId, task.getProjectId()));
        return "task/taskView";
    }

    @GetMapping(value = "/taskedit")
    public String taskEdit(final Model model, @RequestParam(name = "id") final String id) throws Exception {
        model.addAttribute("task", taskService.findOne(userId, id));
        model.addAttribute("statuses", Arrays.asList(Status.values()));
        model.addAttribute("projects", projectService.findAll(userId));
        return "task/taskEdit";
    }

    @PostMapping(value = "/taskmerge")
    public String taskMerge(@ModelAttribute(name = "task") final TaskDTO task) throws Exception {
        taskService.merge(userId, task.getId(), task);
        return "redirect:/tasks";
    }

    @GetMapping(value = "/taskremove")
    public String taskRemove(@RequestParam(name = "id") final String id) throws Exception {
        taskService.remove(userId, id);
        return "redirect:/tasks";
    }

    @GetMapping(value = "/taskcreate")
    public String taskCreate(final Model model) throws Exception {
        final TaskDTO task = new TaskDTO();
        task.setUserId(userId);
        model.addAttribute("task", task);
        model.addAttribute("statuses", Arrays.asList(Status.values()));
        model.addAttribute("projects", projectService.findAll(userId));
        return "task/taskEdit";
    }
}
