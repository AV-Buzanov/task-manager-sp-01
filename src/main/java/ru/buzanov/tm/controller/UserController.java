package ru.buzanov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.buzanov.tm.dto.UserDTO;
import ru.buzanov.tm.entity.User;
import ru.buzanov.tm.repository.UserRepository;
import ru.buzanov.tm.service.UserService;
import ru.buzanov.tm.util.PasswordUtil;

import javax.servlet.http.HttpSession;

@Controller
@SessionAttributes(value = "user")
public class UserController {
    @Autowired
    private UserService userService;

//    @PostMapping("/userauth")
//    public String userAuth(HttpSession httpSession, @RequestParam(name = "login") String login, @RequestParam(name = "password") String password) {
////        if (userRepository.existsByLogin(login)) {
////            User user = userRepository.findByLogin(login);
////            if (user.getPasswordHash().equals(PasswordUtil.hashingPass(password, user.getId())))
////                httpSession.setAttribute("user", user);
////        }
//        UserDTO user = new UserDTO();
//        user.setLogin(login);
//        user.setPasswordHash(password);
//        user.setName("Name");
//        httpSession.setAttribute("user", user);
//        return "main";
//    }
//
    @GetMapping(value = "/userinfo")
    public String userInfo(final Model model) throws Exception {
        final UserDTO user = userService.findOne("0fba2676-0c83-4831-8a16-ca35927ef8d9");
        model.addAttribute("user", user);
        return "user";
    }
}
