package ru.buzanov.tm.util;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.Properties;

@Getter
@Setter
public class PropertyService {
    @NotNull
    private final static Properties jdbc = new Properties();
    @NotNull
    private final static Properties session = new Properties();

    static {
        try {
            jdbc.load(PropertyService.class.getResourceAsStream("/jdbc.properties"));
            session.load(PropertyService.class.getResourceAsStream("/session.properties"));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getJdbcUsername() {
        return jdbc.getProperty("jdbcUser");
    }

    public String getJdbcPassword() {
        return jdbc.getProperty("jdbcPass");
    }

    public String getJdbcDriver() {
        return jdbc.getProperty("jdbcDriver");
    }

    public String getJdbcUrl() {
        return jdbc.getProperty("jdbcAdress");
    }

    public String getJdbcUrl2() {
        return jdbc.getProperty("jdbcAdress2");
    }

    public int getSessionLifetime() {
        return Integer.parseInt(session.getProperty("lifeTime"));
    }

    public int getSignCycle() {
        return Integer.parseInt(session.getProperty("signCycle"));
    }

    public String getSignSalt() {
        return session.getProperty("signSalt");
    }

}
