package ru.buzanov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Entity;

@Getter
@Setter
@NoArgsConstructor
public class TaskDTO extends AbstractWBS {
    @Nullable
    private String projectId;
}