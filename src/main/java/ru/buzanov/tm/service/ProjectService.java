package ru.buzanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.buzanov.tm.dto.ProjectDTO;
import ru.buzanov.tm.entity.Project;
import ru.buzanov.tm.entity.User;
import ru.buzanov.tm.enumerated.Field;
import ru.buzanov.tm.repository.ProjectRepository;

import java.util.*;

@Service
public class ProjectService {
    @Autowired
    @NotNull
    private ProjectRepository projectRepository;

    @Transactional
    public void load(@Nullable final String userId, @Nullable final ProjectDTO project) throws Exception {
        if (project == null || project.getId() == null || project.getName() == null)
            throw new Exception("Argument can't be empty or null");
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        project.setUserId(userId);
        if (project.getCreateDate() == null)
            project.setCreateDate(new Date(System.currentTimeMillis()));
        if (isNameExist(userId, project.getName()))
            throw new Exception("This name already exist!");
        projectRepository.saveAndFlush(toEntity(project));
    }

    @Transactional
    public void load(@Nullable final String userId, @Nullable final List<ProjectDTO> list) throws Exception {
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (list == null)
            throw new Exception("Argument can't be empty or null");
        for (@NotNull final ProjectDTO project : list)
            load(userId, project);
    }

    public @NotNull Collection<ProjectDTO> findAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        int count = 1;
        @NotNull final List<ProjectDTO> list = new ArrayList<>();
        for (@NotNull final Project project : projectRepository.findAllByUserId(userId)) {
            ProjectDTO projectDTO = toDTO(project);
            projectDTO.setCount(count);
            count++;
            list.add(projectDTO);
        }
        return list;
    }

    public @NotNull Collection<ProjectDTO> findByName(@Nullable final String userId, @Nullable final String name) throws Exception {
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (name == null || name.isEmpty())
            throw new Exception("Argument can't be empty or null");
        @NotNull final List<ProjectDTO> list = new ArrayList<>();
        for (@NotNull final Project project : projectRepository.findAllByUserIdAndNameContaining(userId, name))
            list.add(toDTO(project));
        return list;
    }

    public @NotNull Collection<ProjectDTO> findByDescription(@Nullable final String userId, @Nullable final String desc) throws Exception {
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (desc == null || desc.isEmpty())
            throw new Exception("Argument can't be empty or null");
        @NotNull final List<ProjectDTO> list = new ArrayList<>();
        for (@NotNull final Project project : projectRepository.findAllByUserIdAndDescriptionContaining(userId, desc))
            list.add(toDTO(project));
        return list;
    }

    @Nullable
    public ProjectDTO findOne(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (id == null || id.isEmpty())
            throw new Exception("Argument can't be empty or null");
        return toDTO(projectRepository.findByUserIdAndId(userId, id));
    }

    public boolean isNameExist(@Nullable final String userId, @Nullable final String name) throws Exception {
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (name == null || name.isEmpty())
            throw new Exception("Argument can't be empty or null");
        return projectRepository.existsByUserIdAndName(userId, name);
    }

    @Transactional
    public void merge(@Nullable String userId, @Nullable final String id, @Nullable final ProjectDTO project) throws Exception {
        if (project == null || project.getName() == null)
            throw new Exception("Argument can't be empty or null");
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (id == null || id.isEmpty())
            throw new Exception("Argument can't be empty or null");
        Project project1 = projectRepository.findByUserIdAndId(userId, id);
        if (project1 != null &&
                !project1.getName().equals(project.getName()) &&
                isNameExist(userId, project.getName()))
            throw new Exception("This name already exist!");
        if (project1 == null &&
                isNameExist(userId, project.getName()))
            throw new Exception("This name already exist!");
        project.setUserId(userId);
        project.setId(id);
        projectRepository.saveAndFlush(toEntity(project));
    }


    public @NotNull Collection<ProjectDTO> findAllOrdered(@Nullable final String userId, boolean dir, @NotNull final Field field) throws Exception {
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (field == null)
            throw new Exception("Argument can't be empty or null");
        @NotNull List<Project> listEntity = new ArrayList<>();
        @NotNull final List<ProjectDTO> list = new ArrayList<>();
        switch (field) {
            case NAME:
                listEntity = new ArrayList<>(projectRepository.findAllByUserIdOrderByNameAsc(userId));
                break;
            case DESCRIPTION:
                listEntity = new ArrayList<>(projectRepository.findAllByUserIdOrderByDescriptionAsc(userId));
                break;
            case START_DATE:
                listEntity = new ArrayList<>(projectRepository.findAllByUserIdOrderByStartDateAsc(userId));
                break;
            case FINISH_DATE:
                listEntity = new ArrayList<>(projectRepository.findAllByUserIdOrderByFinishDateAsc(userId));
                break;
        }
        if (dir)
            Collections.reverse(listEntity);
        for (@NotNull final Project project : listEntity)
            list.add(toDTO(project));
        return list;
    }

    @Transactional
    public void remove(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (id == null || id.isEmpty())
            throw new Exception("Argument can't be empty or null");
        projectRepository.deleteByUserIdAndId(userId, id);
    }

    @Transactional
    public void removeAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        projectRepository.deleteAllByUserId(userId);
    }

    @Nullable
    public String getList(String userId) throws Exception {
        int indexBuf = 1;
        @NotNull final StringBuilder s = new StringBuilder();
        @NotNull final Collection<ProjectDTO> list = findAll(userId);
        for (@NotNull final ProjectDTO project : list) {
            s.append(indexBuf).append(". ").append(project.getName());
            if (list.size() > indexBuf)
                s.append(System.getProperty("line.separator"));
            indexBuf++;
        }
        return s.toString();
    }

    @Nullable
    public String getIdByCount(@Nullable final String userId, int count) throws Exception {
        if (userId == null || userId.isEmpty())
            return null;
        int indexBuf = 1;
        @NotNull final Collection<ProjectDTO> list = findAll(userId);
        for (@NotNull final ProjectDTO entity : list) {
            if (indexBuf == count)
                return entity.getId();
            indexBuf++;
        }
        return null;
    }

    @Nullable
    private ProjectDTO toDTO(@Nullable final Project project) {
        if (project == null)
            return null;
        @NotNull final ProjectDTO projectDto = new ProjectDTO();
        if (project.getId() != null)
            projectDto.setId(project.getId());
        if (project.getName() != null)
            projectDto.setName(project.getName());
        if (project.getStartDate() != null)
            projectDto.setStartDate(project.getStartDate());
        if (project.getFinishDate() != null)
            projectDto.setFinishDate(project.getFinishDate());
        if (project.getDescription() != null)
            projectDto.setDescription(project.getDescription());
        if (project.getStatus() != null)
            projectDto.setStatus(project.getStatus());
        if (project.getCreateDate() != null)
            projectDto.setCreateDate(project.getCreateDate());
        if (project.getUser() != null)
            projectDto.setUserId(project.getUser().getId());
        return projectDto;
    }

    @Nullable
    private Project toEntity(@Nullable final ProjectDTO project) {
        if (project == null)
            return null;
        @NotNull final Project projectEntity = new Project();
        if (project.getId() != null)
            projectEntity.setId(project.getId());
        if (project.getName() != null)
            projectEntity.setName(project.getName());
        if (project.getStartDate() != null)
            projectEntity.setStartDate(project.getStartDate());
        if (project.getFinishDate() != null)
            projectEntity.setFinishDate(project.getFinishDate());
        if (project.getDescription() != null)
            projectEntity.setDescription(project.getDescription());
        if (project.getStatus() != null)
            projectEntity.setStatus(project.getStatus());
        if (project.getCreateDate() != null)
            projectEntity.setCreateDate(project.getCreateDate());
        if (project.getUserId() != null)
            projectEntity.setUser(new User(project.getUserId()));
        return projectEntity;
    }
}