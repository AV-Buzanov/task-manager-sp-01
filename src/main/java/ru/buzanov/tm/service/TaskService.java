package ru.buzanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.buzanov.tm.dto.TaskDTO;
import ru.buzanov.tm.entity.Project;
import ru.buzanov.tm.entity.Task;
import ru.buzanov.tm.entity.User;
import ru.buzanov.tm.enumerated.Field;
import ru.buzanov.tm.repository.TaskRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Service
public class TaskService {
    @Autowired
    @NotNull
    private TaskRepository taskRepository;

    @Transactional
    public void load(@Nullable final String userId, @Nullable final TaskDTO task) throws Exception {
        if (task == null || task.getId() == null || task.getName() == null)
            throw new Exception("Argument can't be empty or null");
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (isNameExist(userId, task.getName()))
            throw new Exception("This name already exist!");
        task.setUserId(userId);
        taskRepository.saveAndFlush(toEntity(task));
    }

    @Transactional
    public void load(@Nullable String userId, List<TaskDTO> list) throws Exception {
        if (list == null)
            throw new Exception("Argument can't be empty or null");
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        for (@NotNull final TaskDTO task : list)
            load(userId, task);
    }

    public @NotNull Collection<TaskDTO> findAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        @NotNull final List<TaskDTO> list = new ArrayList<>();
        int count = 1;
        for (@NotNull final Task task : taskRepository.findAllByUserId(userId)) {
            TaskDTO taskDto = toDTO(task);
            taskDto.setCount(count);
            count++;
            list.add(taskDto);
        }
        return list;
    }

    public @NotNull Collection<TaskDTO> findByName(@Nullable final String userId, @Nullable final String name) throws Exception {
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (name == null || name.isEmpty())
            throw new Exception("Argument can't be empty or null");
        @NotNull final List<TaskDTO> list = new ArrayList<>();
        for (@NotNull final Task task : taskRepository.findAllByUserIdAndNameContaining(userId, name))
            list.add(toDTO(task));
        return list;
    }

    public @NotNull Collection<TaskDTO> findByDescription(@Nullable final String userId, @Nullable final String desc) throws Exception {
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (desc == null || desc.isEmpty())
            throw new Exception("Argument can't be empty or null");
        @NotNull final List<TaskDTO> list = new ArrayList<>();
        for (@NotNull final Task task : taskRepository.findAllByUserIdAndDescriptionContaining(userId, desc))
            list.add(toDTO(task));
        return list;
    }

    public @NotNull Collection<TaskDTO> findByProjectId(@Nullable final String userId, @Nullable final String projectId) throws Exception {
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (projectId == null || projectId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        @NotNull final List<TaskDTO> list = new ArrayList<>();
        for (@NotNull final Task task : taskRepository.findAllByUserIdAndProjectId(userId, projectId))
            list.add(toDTO(task));
        return list;
    }

    @Nullable
    public TaskDTO findOne(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (id == null || id.isEmpty())
            throw new Exception("Argument can't be empty or null");
        return toDTO(taskRepository.findByUserIdAndId(userId, id));
    }

    public boolean isNameExist(@Nullable final String userId, @Nullable final String name) throws Exception {
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (name == null || name.isEmpty())
            throw new Exception("Argument can't be empty or null");
        return taskRepository.existsByUserIdAndName(userId, name);
    }

    @Transactional
    public void merge(@Nullable final String userId, @Nullable final String id, @Nullable final TaskDTO task) throws Exception {
        if (task == null || task.getName() == null)
            throw new Exception("Argument can't be empty or null");
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (id == null || id.isEmpty())
            throw new Exception("Argument can't be empty or null");
        Task task1 = taskRepository.findByUserIdAndId(userId, id);
        if (task1 != null &&
                !task1.getName().equals(task.getName()) &&
                isNameExist(userId, task.getName()))
            throw new Exception("This name already exist!");
        if (task1 == null &&
                isNameExist(userId, task.getName()))
            throw new Exception("This name already exist!");
        task.setUserId(userId);
        taskRepository.saveAndFlush(toEntity(task));
    }

    @Transactional
    public void remove(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (id == null || id.isEmpty())
            throw new Exception("Argument can't be empty or null");
        taskRepository.deleteByUserIdAndId(userId, id);
    }

    @Transactional
    public void removeAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        taskRepository.deleteAllByUserId(userId);
    }

    @Transactional
    public void removeByProjectId(@Nullable final String userId, @Nullable final String projectId) throws Exception {
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (projectId == null || projectId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        taskRepository.deleteAllByUserIdAndProjectId(userId, projectId);
    }

    public @NotNull Collection<TaskDTO> findAllOrdered(@Nullable final String userId, boolean dir, @NotNull final Field field) throws Exception {
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (field == null)
            throw new Exception("Argument can't be empty or null");
        @NotNull List<Task> listEntity = new ArrayList<>();
        @NotNull final List<TaskDTO> list = new ArrayList<>();
        switch (field) {
            case NAME:
                listEntity = new ArrayList<>(taskRepository.findAllByUserIdOrderByNameAsc(userId));
                break;
            case DESCRIPTION:
                listEntity = new ArrayList<>(taskRepository.findAllByUserIdOrderByDescriptionAsc(userId));
                break;
            case START_DATE:
                listEntity = new ArrayList<>(taskRepository.findAllByUserIdOrderByStartDateAsc(userId));
                break;
            case FINISH_DATE:
                listEntity = new ArrayList<>(taskRepository.findAllByUserIdOrderByFinishDateAsc(userId));
                break;
        }
        if (dir)
            Collections.reverse(listEntity);
        for (@NotNull final Task task : listEntity)
            list.add(toDTO(task));
        return list;
    }

    @Nullable
    public String getList(String userId) throws Exception {
        int indexBuf = 1;
        @NotNull final StringBuilder s = new StringBuilder();
        @NotNull final Collection<TaskDTO> list = findAll(userId);
        for (@NotNull final TaskDTO project : list) {
            s.append(indexBuf).append(". ").append(project.getName());
            if (list.size() > indexBuf)
                s.append(System.getProperty("line.separator"));
            indexBuf++;
        }
        return s.toString();
    }

    @Nullable
    public String getIdByCount(@Nullable final String userId, int count) throws Exception {
        if (userId == null || userId.isEmpty())
            return null;
        int indexBuf = 1;
        @NotNull final Collection<TaskDTO> list = findAll(userId);
        for (@NotNull final TaskDTO entity : list) {
            if (indexBuf == count)
                return entity.getId();
            indexBuf++;
        }
        return null;
    }

    @Nullable
    private TaskDTO toDTO(@Nullable final Task task) {
        if (task == null)
            return null;
        @NotNull final TaskDTO taskDto = new TaskDTO();
        if (task.getId() != null)
            taskDto.setId(task.getId());
        if (task.getName() != null)
            taskDto.setName(task.getName());
        if (task.getStartDate() != null)
            taskDto.setStartDate(task.getStartDate());
        if (task.getFinishDate() != null)
            taskDto.setFinishDate(task.getFinishDate());
        if (task.getDescription() != null)
            taskDto.setDescription(task.getDescription());
        if (task.getStatus() != null)
            taskDto.setStatus(task.getStatus());
        if (task.getCreateDate() != null)
            taskDto.setCreateDate(task.getCreateDate());
        if (task.getUser() != null)
            taskDto.setUserId(task.getUser().getId());
        if (task.getProject() != null)
            taskDto.setProjectId(task.getProject().getId());
        return taskDto;
    }

    @Nullable
    private Task toEntity(@Nullable final TaskDTO task) throws Exception {
        if (task == null)
            return null;
        @NotNull final Task taskEntity = new Task();
        if (task.getId() != null)
            taskEntity.setId(task.getId());
        if (task.getName() != null)
            taskEntity.setName(task.getName());
        if (task.getStartDate() != null)
            taskEntity.setStartDate(task.getStartDate());
        if (task.getFinishDate() != null)
            taskEntity.setFinishDate(task.getFinishDate());
        if (task.getDescription() != null)
            taskEntity.setDescription(task.getDescription());
        if (task.getStatus() != null)
            taskEntity.setStatus(task.getStatus());
        if (task.getCreateDate() != null)
            taskEntity.setCreateDate(task.getCreateDate());
        if (task.getUserId() != null)
            taskEntity.setUser(new User(task.getUserId()));
        if (task.getProjectId() != null)
            taskEntity.setProject(new Project(task.getProjectId()));
        return taskEntity;
    }
}